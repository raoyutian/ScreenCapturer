﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScreenCapturer
{  
    internal enum SizeGrip
    {
        None = 0,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        All
    }
}
