﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScreenCapturer
{ 
    internal enum OperateType
    {
        None = 0,
        DrawRectangle,
        DrawEllipse,
        DrawArrow,
        DrawLine,
        DrawText
    }
}
