﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScreenCapturer
{    
    public enum DrawToolsDockStyle
    {
        None = 0,
        Top,
        BottomUp,
        Bottom
    }
}
