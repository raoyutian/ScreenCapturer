﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScreenCapturer
{   
    public enum DrawStyle
    {
        None = 0,
        Rectangle,
        Ellipse,
        Arrow,
        Text,
        Line
    }
}
