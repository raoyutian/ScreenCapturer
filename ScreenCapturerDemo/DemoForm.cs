﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScreenCapturerDemo
{
    public partial class DemoForm : Form
    {
        public DemoForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
        }

        private void 截图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ScreenCapturer.Wait.Delay(200);
            ScreenCapturer.ScreenCapturerTool screenCapturer = new ScreenCapturer.ScreenCapturerTool();
            if (screenCapturer.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.BackgroundImage = screenCapturer.Image;
            }
            this.Show();
        }

        private void 截屏ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ScreenCapturer.Wait.Delay(200);
            pictureBox1.BackgroundImage = ScreenCapturer.ImageHelper.GetScreenCapture();
            this.Show();

        }

        private void 区域截图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ScreenCapturer.Wait.Delay(200);
          var bmp=  ScreenCapturer.ImageHelper.GetScreenCapture();

            pictureBox1.BackgroundImage = ScreenCapturer.ImageHelper.GetRectBitmap(bmp,new Rectangle(0,0,300,300));
            this.Show();
        }
    }
}
